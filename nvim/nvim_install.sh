#!/usr/bin/env bash
#
# This script will install the prerequisites for nvim

# Install prerequisites
pacman -S --noconfirm \
	neovim \
	nodejs \
	npm \
	python \
	python-pynvim \
	yarn
	
# Neovim postrequisites
npm -g install remark-cli
npm -g install remark-gfm

