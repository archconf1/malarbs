#!/usr/bin/env bash

# Description
# This script configure a newly installed ArchLinux install with basic configuration:
# - timezone
# - hostname
# - user
# - locale
# - network
# - SSH

# Variable declaration
NEW_USER=unset
NEW_HOSTNAME=unset

# Helper
usage()
{
  echo "Usage arch_install_base [ --new-user ] [ --new-hostname ]"
  echo ""
  echo "Variables:"
  echo ""
  echo " --new-user     new user name"
  echo " --new-hostname hostname"
  exit 2
}

# Parsing arguments
PARSED_ARGUMENTS=$(getopt -o '' -a -n arch_install_base --longoptions new-user:,new-hostname: -- "$@")

# Are the arguments valid?
VALID_ARGUMENTS=$?

# If the arguments are not valid then show the usage
if [ "$VALID_ARGUMENTS" != "0" ]; then
  usage
fi

# Set arguments
eval set -- "$PARSED_ARGUMENTS"
while :
do
  case "$1" in
    --new-user) NEW_USER="$2" ; shift 2 ;;
    --new-hostname) NEW_HOSTNAME="$2" ; shift 2 ;;
    # -- means the end of the arguments; drop this, and break out of the while loop
    --) shift; break ;;
    # If invalid options were passed, then getopt should have reported an error,
    # which we checked as VALID_ARGUMENTS when getopt was called...
    *) echo "Unexpected option: $1 - this should not happen."
    usage ;;
esac
done

# Set time
ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime
hwclock --systohc

# Set locale
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "KEYMAP=us" > /etc/vconsole.conf

# Install rEFInd
pacman -S --noconfirm intel-ucode gptfdisk refind efibootmgr mkinitcpio
refind-install

# Set hostname
echo "${NEW_HOSTNAME}" > /etc/hostname
echo -e "127.0.0.1        localhost\n::1              localhost\n127.0.1.1          ${NEW_HOSTNAME}.localdomain ${NEW_HOSTNAME}" >> /etc/hosts

# Add and configure new user
pacman -S --noconfirm sudo
useradd -m -G wheel -s /bin/bash "${NEW_USER}"
sed -i 's/# %wheel/%wheel/g' /etc/sudoers
usermod -a -G users "${NEW_USER}"

# Install and enable dhcp
pacman -S --noconfirm dhcpcd
systemctl enable dhcpcd

# Install and enable openssh
pacman -S --noconfirm openssh
systemctl enable sshd

