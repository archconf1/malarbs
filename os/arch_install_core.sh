#!/usr/bin/env bash

# Description 
# This script will install an new ArchLinux OS with pacstrap and generate the fstab


# Variables declaration

DISK_PATH=unset
BOOT_SIZE=unset
HOME_SIZE=unset
ROOT_SIZE=unset
SWAP_SIZE=unset
CYLINDER=512

# Helper
usage()
{
  echo "This script will install an new ArchLinux OS with pacstrap and generate the fstab"
  echo "Usage: arch_installer_core [ --disk ] [ --boot-size ] [ --home-size ] [ --root-size ] [ --swap-size ]"
  echo ""
  echo "Variables:"
  echo ""
  echo " --disk-path    path to the disk to install Archlinux to"
  echo " --boot-size    size in MB of the boot parition"
  echo " --home-size    size in MB of the home partition"
  echo " --root-size    size in MB of the root partition"
  echo " --swap-size    size in MB of the SWAP partition"
  exit 2
}

# Cylinder count
cylinder_count () {
  mb_size=$1*1000000
  remains=$(($mb_size % $CYLINDER))
  modulo=$(($mb_size-$remains))
  nb_cylinder=$(($modulo/$CYLINDER))
  echo $nb_cylinder
}

# Parsing arguments
PARSED_ARGUMENTS=$(getopt -o '' -a -n arch_installer_core --longoptions disk-path:,boot-size:,home-size:,root-size:,swap-size: -- "$@")

# Are the arguments valid?
VALID_ARGUMENTS=$?

# If the arguments are not valid then show the usage
if [ "$VALID_ARGUMENTS" != "0" ]; then
  usage
fi

# Set arguments
eval set -- "$PARSED_ARGUMENTS"
while :
do
  case "$1" in
    --disk-path) DISK_PATH="$2" ; shift 2 ;;
    --boot-size) BOOT_SIZE="$2" ; shift 2 ;;
    --home-size) HOME_SIZE="$2" ; shift 2 ;;
    --root-size) ROOT_SIZE="$2" ; shift 2 ;;
    --swap-size) SWAP_SIZE="$2" ; shift 2 ;;
    # -- means the end of the arguments; drop this, and break out of the while loop
    --) shift; break ;;
    # If invalid options were passed, then getopt should have reported an error,
    # which we checked as VALID_ARGUMENTS when getopt was called...
    *) echo "Unexpected option: $1 - this should not happen."
    usage ;;
esac
done

# Set time
timedatectl set-ntp true

# Load keyboard layout
loadkeys fr_CH-latin1

# Create partition
sfdisk -f $DISK_PATH << EOF
,$(cylinder_count $BOOT_SIZE), U
,$(cylinder_count $SWAP_SIZE), S
,$(cylinder_count $ROOT_SIZE), L
,$(cylinder_count $HOME_SIZE), L
;
EOF

# Format partitions
mkfs.fat -F32 "${DISK_PATH}1"
mkswap "${DISK_PATH}2"
mkfs.ext4 -F "${DISK_PATH}3"
mkfs.ext4 -F "${DISK_PATH}4"

# Mount partitions
mkdir /mnt/efi
mount "${DISK_PATH}1" /mnt/efi
swapon "${DISK_PATH}2"
mount "${DISK_PATH}3" /mnt
mkdir /mnt/home
mount "${DISK_PATH}4" /mnt/home

# Install Archlinux
pacstrap /mnt base base-devel linux linux-firmware

# Generate fstab
genfstab -U /mnt >> /mnt/etc/fstab

