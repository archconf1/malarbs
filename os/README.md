# Arch install

Scripts to automate the installation of an ArchLinux distribution.

## Installation

Two scripts are provided. The _arch_install_core.sh_ script provides the core install for ArchLinux. The _arch_install_base.sh_ sript provides the base installation for ArchLinux.

### arch_install_core

Download the script from Gitlab:

    curl -O -k https://gitlab.com/cybernemo/.dotfiles/-/raw/main/arch/arch_install_core.sh

## Usage

    bash arch_install_core.sh --disk /dev/SDX --boot-size XXXX --home-size YYYY --root-size ZZZZ --swap-size AAAA

### arch_install_base

Chroot into the new installation:

    arch-chroot /mnt

Download the script from Gitlab:

    curl -O -k https://gitlab.com/cybernemo/.dotfiles/-/raw/main/arch/arch_install_base.sh

## Usage

    bash arch_install_base.sh --new-user XXX --new-hostname YYY

## Post-install

Change the new user password:

    passwd NEW_USER

Change the root password:

    passwd

## Dell xps 9350

### BIOS

After installation, enter the BIOS under _Boot Sequence_, click on _Add Boot Option_, set a name and select file under _EFI/refind/refind_x64.efi_. Apply the changes and reboot.

### rEFInd

After booting in the rEFInd boot manager, click on TAB and use _Boot with minimal options_ in order to be able to boot.
