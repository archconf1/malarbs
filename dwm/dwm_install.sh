#!/usr/bin/env bash

# Variable declaration
USER=unset

# Helper
usage()
{
  echo "Usage arch_install_ui [ --user ]"
  echo ""
  echo "Variables:"
  echo ""
  echo " --user     user to install to"
  exit 2
}

# Parsing arguments
PARSED_ARGUMENTS=$(getopt -o '' -a -n arch_install_base --longoptions user: -- "$@")

# Are the arguments valid?
VALID_ARGUMENTS=$?

# If the arguments are not valid then show the usage
if [ "$VALID_ARGUMENTS" != "0" ]; then
  usage
fi

# Set arguments
eval set -- "$PARSED_ARGUMENTS"
while :
do
  case "$1" in
    --user) USER="$2" ; shift 2 ;;
    # -- means the end of the arguments; drop this, and break out of the while loop
    --) shift; break ;;
    # If invalid options were passed, then getopt should have reported an error,
    # which we checked as VALID_ARGUMENTS when getopt was called...
    *) echo "Unexpected option: $1 - this should not happen."
    usage ;;
esac
done

# Install needed packages
pacman -S --noconfirm pkgconf make gcc libxinerama libxft

# Create config folder
mkdir -p "/home/${USER}/.config"

# Git clone all repositories
git clone https://gitlab.com:ARBS1/dwm.git "/home/${USER}/.config/dwm"
git clone https://gitlab.com:ARBS1/st.git "/home/${USER}/.config/st"
git clone https://gitlab.com:ARBS1/dmenu.git "/home/${USER}/.config/dmenu"
git clone https://gitlab.com:ARBS1/dwmblocks.git "/home/${USER}/.config/dwmblocks"

# Compile repositories
cd "/home/${USER}/.config/dwm" && make clean install
cd "/home/${USER}/.config/st" && make clean install
cd "/home/${USER}/.config/dmenu" && make clean install
cd "/home/${USER}/.config/dwmblocks" && make clean install
chown -R "${USER}:${USER}" "/home/${USER}/.config/"

# Configure lightdm
mkdir /usr/share/xsessions
touch /usr/share/xsessions/dwm.desktop
cat >> /usr/share/xsessions/dwm.desktop<< EOF
[Desktop Entry]
Encoding=UTF-8
Name=Dwm
Comment=the dynamic window manager
Exec=dwm
Icon=dwm
Type=XSession
EOF
