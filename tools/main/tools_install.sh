#!/usr/bin/env bash
#
# This script will install standard tools

# Install tools 
pacman -S --noconfirm \
	acpid \
	bc \
	bat \
	bmon \
	cpupower \
	dnsutils \
	feh \
	git \
	htop \
	lm_sensors \
	lsd \
	networkmanager \
	nmap \
	pamixer \
	pulseaudio \
	ranger \
	stow \
	upower \
	wget \
	xbindkeys \
	xclip 

# Start and enable tools
systemctl enable NetworkManager.service
systemctl start NetworkManager.service

systemctl enable acpid 
systemctl start acpid 


