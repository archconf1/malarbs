#!/usr/bin/env bash
# 
# This script installs the virtualization tools

# Variable declaration
USER=unset

# Helper
usage()
{
  echo "Usage virtualization_install [ --user ]"
  echo ""
  echo "Variables:"
  echo ""
  echo " --user     user to install to"
  exit 2
}

# Parsing arguments
PARSED_ARGUMENTS=$(getopt -o '' -a -n arch_install_base --longoptions user: -- "$@")

# Are the arguments valid?
VALID_ARGUMENTS=$?

# If the arguments are not valid then show the usage
if [ "$VALID_ARGUMENTS" != "0" ]; then
  usage
fi

# Set arguments
eval set -- "$PARSED_ARGUMENTS"
while :
do
  case "$1" in
    --user) USER="$2" ; shift 2 ;;
    # -- means the end of the arguments; drop this, and break out of the while loop
    --) shift; break ;;
    # If invalid options were passed, then getopt should have reported an error,
    # which we checked as VALID_ARGUMENTS when getopt was called...
    *) echo "Unexpected option: $1 - this should not happen."
    usage ;;
esac
done

# Install needed packages
pacman -S --noconfirm \
  cockpit \
  cockpit-machines\
  docker \
  docker-compose \
  qemu-full \
  terraform \
  libvirt \
  udisks2 \
  virt-manager

# Configure Cockpit
systemctl start cockpit.socket
systemctl enable cockpit.socket

# Configure Docker
systemctl start docker
systemctl enable docker
getent group docker || groupadd docker
usermod -aG docker $USER

# Configure libvirt
systemctl start libvirtd
systemctl enable libvirtd
usermod -aG libvirt $USER
