#!/usr/bin/env bash
# 
# This script install and configure X11

# Variable declaration
USER=unset

# Helper
usage()
{
  echo "Usage X11_install [ --user ]"
  echo ""
  echo "Variables:"
  echo ""
  echo " --user     user to install to"
  exit 2
}

# Parsing arguments
PARSED_ARGUMENTS=$(getopt -o '' -a -n arch_install_base --longoptions user: -- "$@")

# Are the arguments valid?
VALID_ARGUMENTS=$?

# If the arguments are not valid then show the usage
if [ "$VALID_ARGUMENTS" != "0" ]; then
  usage
fi

# Set arguments
eval set -- "$PARSED_ARGUMENTS"
while :
do
  case "$1" in
    --user) USER="$2" ; shift 2 ;;
    # -- means the end of the arguments; drop this, and break out of the while loop
    --) shift; break ;;
    # If invalid options were passed, then getopt should have reported an error,
    # which we checked as VALID_ARGUMENTS when getopt was called...
    *) echo "Unexpected option: $1 - this should not happen."
    usage ;;
esac
done

# Install Xorg
pacman -S --noconfirm xorg-server xorg-xinit xorg-xrandr xorg-xsetroot acpilight 

# Add user to video group
usermod -a -G video $USER
# Install libinput
pacman -S --noconfirm xf86-input-libinput

# Configure touchpad
cp config/30-touchpad.conf /etc/X11/xorg.conf.d/30-touchpad.conf
