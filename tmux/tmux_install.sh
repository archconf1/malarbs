#!/usr/bin/env bash
#
# This script install the prerequisite for tmux

# Install tumux
pacman -S --noconfirm \
	tmux \
	powerline \
	powerline-fonts
