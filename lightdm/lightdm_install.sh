#!/usr/bin/env bash
#
# This script installs lightdm greeter

# Helper
usage()
{
  echo "Usage lightdm_install"
  echo ""
  exit 2
}

# Install lightdm
pacman -S --noconfirm \
  lightdm \
  lightdm-gtk-greeter \
  lightdm-gtk-greeter-settings

# Enable lightdm
systemctl enable lightdm
