#!/usr/bin/env bash
#
# This script will install the prerequisites for zsh 

# Helper
usage()
{
  echo "Usage zsh_install [ --user ]"
  echo ""
  echo "Variables:"
  echo ""
  echo " --user     user to install to"
  exit 2
}

# Parsing arguments
PARSED_ARGUMENTS=$(getopt -o '' -a -n zsh_install --longoptions user: -- "$@")

# Are the arguments valid?
VALID_ARGUMENTS=$?

# If the arguments are not valid then show the usage
if [ "$VALID_ARGUMENTS" != "0" ]; then
  usage
fi

# Set arguments
eval set -- "$PARSED_ARGUMENTS"
while :
do
  case "$1" in
    --user) USER="$2" ; shift 2 ;;
    # -- means the end of the arguments; drop this, and break out of the while loop
    --) shift; break ;;
    # If invalid options were passed, then getopt should have reported an error,
    # which we checked as VALID_ARGUMENTS when getopt was called...
    *) echo "Unexpected option: $1 - this should not happen."
    usage ;;
esac
done

# Install zsh
pacman -S --noconfirm \
	zsh

# ZSH postrequisites
pacman -S --noconfirm \
	fzf \
	wget

mkdir /usr/bin/fzf
wget https://raw.githubusercontent.com/junegunn/fzf/master/shell/completion.zsh -P /usr/share/fzf/
wget https://raw.githubusercontent.com/junegunn/fzf/master/shell/key-bindings.zsh -P /usr/share/fzf/
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions
chsh -s /usr/bin/zsh root
chsh -s /usr/bin/zsh $USER

